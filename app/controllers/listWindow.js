

//データバインドを行う
function transformFunction(model) {
    var transform = model.toJSON();
    return transform;
}

//表示データを絞り込み
function filterFunction(collection) {
	if(Alloy.Globals.hiduke==='')
	{
		//return collection.where({hiduke:Alloy.Globals.hiduke});
	}
	else{
    	return collection.where({hiduke:Alloy.Globals.hiduke});
    }
}

var moneys = Alloy.Collections.money;
moneys.fetch();

//画面を閉じる
function cancel() {
	$.destroy();
	Alloy.Globals.navwin.closeWindow($.listWindow);
}

//データ一覧を表示する
function ListWindow() {
	var moneys = Alloy.createCollection('money');
  	moneys.selectItems();
}

//選択した明細を編集画面に表示する
function modifyData(e){
	//選択した明細のIDをグローバル変数にセットする
	Alloy.Globals.id = e.row.cd;

	//バインド情報を解散する
	$.destroy();
	Alloy.Globals.navwin.closeWindow($.listWindow);

	var modifyWindowController = Alloy.createController('modifyWindow', {parent:$});
    Alloy.Globals.navwin.openWindow(modifyWindowController.getView());
}
