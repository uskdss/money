
//選択されたデータを編集するために画面に表示する
function dispData() {

  var moneys = Alloy.Collections.money;
  moneys.fetch({id:Alloy.Globals.id});//該当データ
  
  for(var i = 0; i < moneys.length; i ++){
    if(moneys.at(i).get('id')===Alloy.Globals.id){
      break;
    }
  }

  //画面情報を取得する
  $.itemID.text = moneys.at(i).get('id');
  $.itemHIDUKE.value = moneys.at(i).get('hiduke');
  $.itemKINGAKU.value = moneys.at(i).get('kingaku');
  $.itemBIKOU.value = moneys.at(i).get('bikou');

}

//編集したデータをDBに更新する
function modifyData(e) {

　//入力チェックを行う
  if ($.itemHIDUKE.value === '') {
    alert('消費日付を入力してください');
    return; 
  }
  if ($.itemKINGAKU.value === '') {
    alert('消費金額を入力してください');
    return; 
  }

   var moneys = Alloy.createCollection('money');

  //画面情報を取得してCollectionにセットする
  moneys.id = $.itemID.text;
  moneys.hiduke = $.itemHIDUKE.value;
  moneys.kingaku = $.itemKINGAKU.value;
  moneys.bikou = $.itemBIKOU.value;
 
　//DB更新用メッソドを呼び出す 
  moneys.modifyData();
　
//画面を閉じる
  Alloy.Globals.navwin.closeWindow($.modifyWindow);
}

//該当データを削除する
function deleteData(e) {

  var moneys = Alloy.createCollection('money');
  moneys.deleteData();

  $.destroy();
  Alloy.Globals.navwin.closeWindow($.modifyWindow);
}

//編集をキャンセルする 
function cancel(e) {
  $.destroy();
	Alloy.Globals.navwin.closeWindow($.modifyWindow);
}

