
//データを追加する
function addData(e) {
  //入力チェックを行う
	if ($.itemHIDUKE.value === '') {
		alert('消費日付を入力してください');
		return;	
	}
	if ($.itemKINGAKU.value === '') {
		alert('消費金額を入力してください');
		return;	
	}

  var moneys = Alloy.createCollection('money');
  
  //画面に入力した情報をCollectionのプロパティにセットする
  moneys.hiduke = $.itemHIDUKE.value;
  moneys.kingaku = $.itemKINGAKU.value;
  moneys.bikou = $.itemBIKOU.value;
  
  //データ追加のDB更新を呼び出す
  moneys.addData();

  Alloy.Globals.navwin.closeWindow($.addWindow);
}

//画面を閉じる
function cancel(e) {
	Alloy.Globals.navwin.closeWindow($.addWindow);
}

