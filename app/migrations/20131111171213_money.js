migration.up = function(db) {
  db.createTable({
    columns: {
      'id':           'integer not null primary key',
      'hiduke':       'integer',
      'kingaku':      'text',
      'bikou':        'text'
    }
  });
};

migration.down = function(db) {
  db.dropTable();
};
