exports.definition = {
  config: {
    columns: {
      'id':             'integer not null primary key',
      'hiduke':              'text',
      'kingaku':             'integer',
      'bikou':               'text'
    },
    adapter: {
      type:             'sql',
      collection_name:  'money',
      idAttribute:      'id'
    }
  },
  extendModel: function(Model) {
    _.extend(Model.prototype, {
      // extended functions and properties go here
    });

    return Model;
  },
  extendCollection: function(Collection) {
    _.extend(Collection.prototype, {
      // extended functions and properties go here

      //対象日付の消費データを取得する
      selectItems: function() {
        var collection = this;
        var sql = 'select * from ' + collection.config.adapter.collection_name + ' ' +
                  'where hiduke = "' + Alloy.Globals.hiduke + '" ';
        collection.fetch({query: sql});
        collection.trigger('sync');
      },

      //消費データを追加する
      addData : function() {
        var collection = this;
        var sql = "insert into " + collection.config.adapter.collection_name + "(hiduke,kingaku,bikou) values('" + collection.hiduke + "'," + collection.kingaku + ",'" + collection.bikou + "')";
        db = Ti.Database.open(collection.config.adapter.db_name)
        db.execute(sql);
        db.close();
      },

      //消費データを更新する
      modifyData : function() {
        var collection = this;
        var sql = "update " + collection.config.adapter.collection_name + " set hiduke= '" + collection.hiduke + "',kingaku=" + collection.kingaku + ",bikou='" + collection.bikou + "' where id =" + Alloy.Globals.id;
        db = Ti.Database.open(collection.config.adapter.db_name)
        db.execute(sql);
        db.close();
      },
      
      //消費データを削除する
      deleteData : function() {
        var collection = this;
        var sql = "delete from " + collection.config.adapter.collection_name + " where id =" + Alloy.Globals.id;
        db = Ti.Database.open(collection.config.adapter.db_name)
        db.execute(sql);
        db.close();
      }

    });
    return Collection;
  }
};