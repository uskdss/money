// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

Alloy.Globals.navwin = '';
Alloy.Globals.subnavwin = '';

var sysDate = new Date();
var aYear = sysDate.getFullYear();
var aMonth = sysDate.getMonth()+1;
aMonth = (aMonth<10)?'0'+ aMonth:aMonth;
var aDate = sysDate.getDate();
aDate = (aDate<10)?'0'+ aDate:aDate;
Alloy.Globals.hiduke = aYear + '/' + aMonth + '/' + aDate;

Alloy.Globals.id = '';

Alloy.Globals.theme = {
  pittaly_green:    '#009944',
  pittaly_red:      '#E60012',
  pittaly_gray:     '#B5B6B6',
  background:       '#EFEFF4',
  fontcolor:        '#333333',
  defaultcolor:     '#ffffff'
};