var Alloy = require("alloy"), _ = Alloy._, Backbone = Alloy.Backbone;

Alloy.Globals.navwin = "";

Alloy.Globals.subnavwin = "";

var sysDate = new Date();

var aYear = sysDate.getFullYear();

var aMonth = sysDate.getMonth() + 1;

aMonth = 10 > aMonth ? "0" + aMonth : aMonth;

var aDate = sysDate.getDate();

aDate = 10 > aDate ? "0" + aDate : aDate;

Alloy.Globals.hiduke = aYear + "/" + aMonth + "/" + aDate;

Alloy.Globals.id = "";

Alloy.Globals.theme = {
    pittaly_green: "#009944",
    pittaly_red: "#E60012",
    pittaly_gray: "#B5B6B6",
    background: "#EFEFF4",
    fontcolor: "#333333",
    defaultcolor: "#ffffff"
};

Alloy.createController("index");