function Controller() {
    function addData() {
        if ("" === $.itemHIDUKE.value) {
            alert("消費日付を入力してください");
            return;
        }
        if ("" === $.itemKINGAKU.value) {
            alert("消費金額を入力してください");
            return;
        }
        var moneys = Alloy.createCollection("money");
        moneys.hiduke = $.itemHIDUKE.value;
        moneys.kingaku = $.itemKINGAKU.value;
        moneys.bikou = $.itemBIKOU.value;
        moneys.addData();
        Alloy.Globals.navwin.closeWindow($.addWindow);
    }
    function cancel() {
        Alloy.Globals.navwin.closeWindow($.addWindow);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "addWindow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.addWindow = Ti.UI.createWindow({
        backgroundColor: "#fff",
        translucent: false,
        tintColor: Alloy.Globals.theme.pittaly_red,
        navTintColor: Alloy.Globals.theme.pittaly_red,
        modal: true,
        title: "消費記録",
        id: "addWindow",
        exitOnClose: "true"
    });
    $.__views.addWindow && $.addTopLevelView($.__views.addWindow);
    $.__views.addButton = Ti.UI.createButton({
        title: "追加",
        id: "addButton"
    });
    addData ? $.__views.addButton.addEventListener("click", addData) : __defers["$.__views.addButton!click!addData"] = true;
    $.__views.addWindow.leftNavButton = $.__views.addButton;
    $.__views.cancelButton = Ti.UI.createButton({
        title: "取消",
        id: "cancelButton"
    });
    cancel ? $.__views.cancelButton.addEventListener("click", cancel) : __defers["$.__views.cancelButton!click!cancel"] = true;
    $.__views.addWindow.rightNavButton = $.__views.cancelButton;
    $.__views.itemHIDUKE = Ti.UI.createTextField({
        width: "300dp",
        height: "45dp",
        top: "20dp",
        textAlign: "center",
        hintText: "消費日付:yyyy/MM/dd",
        lenth: "10",
        value: Alloy.Globals.hiduke,
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        keyboardType: Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION,
        returnKeyType: Ti.UI.RETURNKEY_DONE,
        id: "itemHIDUKE"
    });
    $.__views.addWindow.add($.__views.itemHIDUKE);
    $.__views.itemKINGAKU = Ti.UI.createTextField({
        width: "300dp",
        height: "45dp",
        top: "70dp",
        textAlign: "right",
        hintText: "消費金額",
        value: "",
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        keyboardType: Titanium.UI.KEYBOARD_DECIMAL_PAD,
        returnKeyType: Ti.UI.RETURNKEY_DONE,
        id: "itemKINGAKU"
    });
    $.__views.addWindow.add($.__views.itemKINGAKU);
    $.__views.itemBIKOU = Ti.UI.createTextField({
        width: "300dp",
        height: "45dp",
        top: "120dp",
        hintText: "消費備考",
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        returnKeyType: Ti.UI.RETURNKEY_DONE,
        id: "itemBIKOU"
    });
    $.__views.addWindow.add($.__views.itemBIKOU);
    exports.destroy = function() {};
    _.extend($, $.__views);
    __defers["$.__views.addButton!click!addData"] && $.__views.addButton.addEventListener("click", addData);
    __defers["$.__views.cancelButton!click!cancel"] && $.__views.cancelButton.addEventListener("click", cancel);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;