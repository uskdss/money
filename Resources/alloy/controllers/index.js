function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.__alloyId2 = Alloy.createController("mainWindow", {
        id: "__alloyId2"
    });
    $.__views.navwin = Ti.UI.iOS.createNavigationWindow({
        window: $.__views.__alloyId2.getViewEx({
            recurse: true
        }),
        id: "navwin"
    });
    $.__views.navwin && $.addTopLevelView($.__views.navwin);
    exports.destroy = function() {};
    _.extend($, $.__views);
    Alloy.Globals.navwin = $.navwin;
    Alloy.Globals.navwin.open({
        transition: Ti.UI.iPhone.AnimationStyle.CURL_UP
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;