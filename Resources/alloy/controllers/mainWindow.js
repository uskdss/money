function Controller() {
    function addTask() {
        Alloy.Globals.hiduke = $.itemHIDUKE.value;
        Alloy.Globals.id = "";
        var addWindowController = Alloy.createController("addWindow", {
            parent: $
        });
        Alloy.Globals.navwin.openWindow(addWindowController.getView());
    }
    function seekTask() {
        Alloy.Globals.hiduke = $.itemHIDUKE.value;
        Alloy.Globals.id = "";
        var addWindowController = Alloy.createController("listWindow", {
            parent: $
        });
        Alloy.Globals.navwin.openWindow(addWindowController.getView());
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "mainWindow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.mainwindow = Ti.UI.createWindow({
        backgroundColor: "white",
        translucent: false,
        tintColor: Alloy.Globals.theme.pittaly_red,
        navTintColor: Alloy.Globals.theme.pittaly_red,
        id: "mainwindow"
    });
    $.__views.mainwindow && $.addTopLevelView($.__views.mainwindow);
    $.__views.itemHIDUKE = Ti.UI.createTextField({
        width: "300dp",
        height: "45dp",
        top: "20dp",
        textAlign: "center",
        hintText: "消費日付:yyyy/MM/dd",
        lenth: "10",
        value: Alloy.Globals.hiduke,
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        keyboardType: Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION,
        id: "itemHIDUKE"
    });
    $.__views.mainwindow.add($.__views.itemHIDUKE);
    $.__views.seekButton = Ti.UI.createButton({
        title: "検索",
        width: "300dp",
        height: "40dp",
        top: "100dp",
        id: "seekButton"
    });
    $.__views.mainwindow.add($.__views.seekButton);
    seekTask ? $.__views.seekButton.addEventListener("click", seekTask) : __defers["$.__views.seekButton!click!seekTask"] = true;
    $.__views.addButton = Ti.UI.createButton({
        title: "追加",
        width: "300dp",
        height: "40dp",
        top: "150dp",
        id: "addButton"
    });
    $.__views.mainwindow.add($.__views.addButton);
    addTask ? $.__views.addButton.addEventListener("click", addTask) : __defers["$.__views.addButton!click!addTask"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    __defers["$.__views.seekButton!click!seekTask"] && $.__views.seekButton.addEventListener("click", seekTask);
    __defers["$.__views.addButton!click!addTask"] && $.__views.addButton.addEventListener("click", addTask);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;