function Controller() {
    function __alloyId14() {
        __alloyId14.opts || {};
        var models = filterFunction(__alloyId13);
        var len = models.length;
        var rows = [];
        for (var i = 0; len > i; i++) {
            var __alloyId7 = models[i];
            __alloyId7.__transform = transformFunction(__alloyId7);
            var __alloyId8 = Ti.UI.createTableViewRow({
                backgroundColor: Alloy.Globals.theme.defaultcolor,
                height: "50dp",
                selectedBackgroundColor: Alloy.Globals.theme.pittaly_green,
                cd: "undefined" != typeof __alloyId7.__transform["id"] ? __alloyId7.__transform["id"] : __alloyId7.get("id")
            });
            rows.push(__alloyId8);
            var __alloyId9 = Ti.UI.createLabel({
                font: {
                    fontSize: 16,
                    fontWeight: "bold"
                },
                top: "3dp",
                left: "9dp",
                text: "undefined" != typeof __alloyId7.__transform["hiduke"] ? __alloyId7.__transform["hiduke"] : __alloyId7.get("hiduke")
            });
            __alloyId8.add(__alloyId9);
            var __alloyId10 = Ti.UI.createLabel({
                font: {
                    fontSize: 16,
                    fontWeight: "bold"
                },
                top: "3dp",
                left: "235dp",
                text: "￥"
            });
            __alloyId8.add(__alloyId10);
            var __alloyId11 = Ti.UI.createLabel({
                font: {
                    fontSize: 16,
                    fontWeight: "bold"
                },
                top: "3dp",
                left: "250dp",
                text: "undefined" != typeof __alloyId7.__transform["kingaku"] ? __alloyId7.__transform["kingaku"] : __alloyId7.get("kingaku")
            });
            __alloyId8.add(__alloyId11);
            var __alloyId12 = Ti.UI.createLabel({
                font: {
                    fontSize: 16
                },
                top: "29dp",
                left: "10dp",
                text: "undefined" != typeof __alloyId7.__transform["bikou"] ? __alloyId7.__transform["bikou"] : __alloyId7.get("bikou")
            });
            __alloyId8.add(__alloyId12);
        }
        $.__views.__alloyId6.setData(rows);
    }
    function transformFunction(model) {
        var transform = model.toJSON();
        return transform;
    }
    function filterFunction(collection) {
        if ("" !== Alloy.Globals.hiduke) return collection.where({
            hiduke: Alloy.Globals.hiduke
        });
    }
    function cancel() {
        $.destroy();
        Alloy.Globals.navwin.closeWindow($.listWindow);
    }
    function modifyData(e) {
        Alloy.Globals.id = e.row.cd;
        $.destroy();
        Alloy.Globals.navwin.closeWindow($.listWindow);
        var modifyWindowController = Alloy.createController("modifyWindow", {
            parent: $
        });
        Alloy.Globals.navwin.openWindow(modifyWindowController.getView());
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "listWindow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    Alloy.Collections.instance("money");
    $.__views.listWindow = Ti.UI.createWindow({
        backgroundColor: Alloy.Globals.theme.background,
        translucent: false,
        tintColor: Alloy.Globals.theme.pittaly_red,
        navTintColor: Alloy.Globals.theme.pittaly_red,
        title: "消費記録",
        id: "listWindow",
        exitOnClose: "true"
    });
    $.__views.listWindow && $.addTopLevelView($.__views.listWindow);
    $.__views.cancelButton = Ti.UI.createButton({
        title: "戻る",
        id: "cancelButton"
    });
    cancel ? $.__views.cancelButton.addEventListener("click", cancel) : __defers["$.__views.cancelButton!click!cancel"] = true;
    $.__views.listWindow.leftNavButton = $.__views.cancelButton;
    $.__views.__alloyId6 = Ti.UI.createTableView({
        scrollable: true,
        backgroundColor: "transparent",
        id: "__alloyId6"
    });
    $.__views.listWindow.add($.__views.__alloyId6);
    var __alloyId13 = Alloy.Collections["money"] || money;
    __alloyId13.on("fetch destroy change add remove reset", __alloyId14);
    modifyData ? $.__views.__alloyId6.addEventListener("click", modifyData) : __defers["$.__views.__alloyId6!click!modifyData"] = true;
    exports.destroy = function() {
        __alloyId13.off("fetch destroy change add remove reset", __alloyId14);
    };
    _.extend($, $.__views);
    var moneys = Alloy.Collections.money;
    moneys.fetch();
    __defers["$.__views.cancelButton!click!cancel"] && $.__views.cancelButton.addEventListener("click", cancel);
    __defers["$.__views.__alloyId6!click!modifyData"] && $.__views.__alloyId6.addEventListener("click", modifyData);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;