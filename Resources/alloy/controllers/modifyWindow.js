function Controller() {
    function dispData() {
        var moneys = Alloy.Collections.money;
        moneys.fetch({
            id: Alloy.Globals.id
        });
        for (var i = 0; moneys.length > i; i++) if (moneys.at(i).get("id") === Alloy.Globals.id) break;
        $.itemID.text = moneys.at(i).get("id");
        $.itemHIDUKE.value = moneys.at(i).get("hiduke");
        $.itemKINGAKU.value = moneys.at(i).get("kingaku");
        $.itemBIKOU.value = moneys.at(i).get("bikou");
    }
    function modifyData() {
        if ("" === $.itemHIDUKE.value) {
            alert("消費日付を入力してください");
            return;
        }
        if ("" === $.itemKINGAKU.value) {
            alert("消費金額を入力してください");
            return;
        }
        var moneys = Alloy.createCollection("money");
        moneys.id = $.itemID.text;
        moneys.hiduke = $.itemHIDUKE.value;
        moneys.kingaku = $.itemKINGAKU.value;
        moneys.bikou = $.itemBIKOU.value;
        moneys.modifyData();
        Alloy.Globals.navwin.closeWindow($.modifyWindow);
    }
    function deleteData() {
        var moneys = Alloy.createCollection("money");
        moneys.deleteData();
        $.destroy();
        Alloy.Globals.navwin.closeWindow($.modifyWindow);
    }
    function cancel() {
        $.destroy();
        Alloy.Globals.navwin.closeWindow($.modifyWindow);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "modifyWindow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.modifyWindow = Ti.UI.createWindow({
        backgroundColor: "#fff",
        translucent: false,
        tintColor: Alloy.Globals.theme.pittaly_red,
        navTintColor: Alloy.Globals.theme.pittaly_red,
        modal: true,
        title: "消費記録",
        id: "modifyWindow",
        exitOnClose: "true"
    });
    $.__views.modifyWindow && $.addTopLevelView($.__views.modifyWindow);
    dispData ? $.__views.modifyWindow.addEventListener("open", dispData) : __defers["$.__views.modifyWindow!open!dispData"] = true;
    $.__views.cancelButton = Ti.UI.createButton({
        title: "戻る",
        id: "cancelButton"
    });
    cancel ? $.__views.cancelButton.addEventListener("click", cancel) : __defers["$.__views.cancelButton!click!cancel"] = true;
    $.__views.modifyWindow.leftNavButton = $.__views.cancelButton;
    $.__views.itemID = Ti.UI.createLabel({
        width: "300dp",
        height: "45dp",
        top: "20dp",
        textAlign: "right",
        id: "itemID"
    });
    $.__views.modifyWindow.add($.__views.itemID);
    $.__views.itemHIDUKE = Ti.UI.createTextField({
        width: "300dp",
        height: "45dp",
        top: "70dp",
        textAlign: "center",
        hintText: "消費日付:yyyy/MM/dd",
        lenth: "10",
        value: Alloy.Globals.hiduke,
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        keyboardType: Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION,
        returnKeyType: Ti.UI.RETURNKEY_DONE,
        id: "itemHIDUKE"
    });
    $.__views.modifyWindow.add($.__views.itemHIDUKE);
    $.__views.itemKINGAKU = Ti.UI.createTextField({
        width: "300dp",
        height: "45dp",
        top: "120dp",
        textAlign: "right",
        hintText: "消費金額",
        value: "",
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        keyboardType: Titanium.UI.KEYBOARD_DECIMAL_PAD,
        returnKeyType: Ti.UI.RETURNKEY_DONE,
        id: "itemKINGAKU"
    });
    $.__views.modifyWindow.add($.__views.itemKINGAKU);
    $.__views.itemBIKOU = Ti.UI.createTextField({
        width: "300dp",
        height: "45dp",
        top: "170dp",
        hintText: "消費備考",
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        returnKeyType: Ti.UI.RETURNKEY_DONE,
        id: "itemBIKOU"
    });
    $.__views.modifyWindow.add($.__views.itemBIKOU);
    $.__views.modifyButton = Ti.UI.createButton({
        title: "変更",
        top: "250dp",
        id: "modifyButton"
    });
    $.__views.modifyWindow.add($.__views.modifyButton);
    modifyData ? $.__views.modifyButton.addEventListener("click", modifyData) : __defers["$.__views.modifyButton!click!modifyData"] = true;
    $.__views.deleteButton = Ti.UI.createButton({
        title: "削除",
        top: "290dp",
        id: "deleteButton"
    });
    $.__views.modifyWindow.add($.__views.deleteButton);
    deleteData ? $.__views.deleteButton.addEventListener("click", deleteData) : __defers["$.__views.deleteButton!click!deleteData"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    __defers["$.__views.modifyWindow!open!dispData"] && $.__views.modifyWindow.addEventListener("open", dispData);
    __defers["$.__views.cancelButton!click!cancel"] && $.__views.cancelButton.addEventListener("click", cancel);
    __defers["$.__views.modifyButton!click!modifyData"] && $.__views.modifyButton.addEventListener("click", modifyData);
    __defers["$.__views.deleteButton!click!deleteData"] && $.__views.deleteButton.addEventListener("click", deleteData);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;