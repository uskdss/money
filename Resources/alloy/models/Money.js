exports.definition = {
    config: {
        columns: {
            id: "integer not null primary key",
            hiduke: "text",
            kingaku: "integer",
            bikou: "text"
        },
        adapter: {
            type: "sql",
            collection_name: "money",
            idAttribute: "id"
        }
    },
    extendModel: function(Model) {
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {
            selectItems: function() {
                var collection = this;
                var sql = "select * from " + collection.config.adapter.collection_name + " " + 'where hiduke = "' + Alloy.Globals.hiduke + '" ';
                collection.fetch({
                    query: sql
                });
                collection.trigger("sync");
            },
            addData: function() {
                var collection = this;
                var sql = "insert into " + collection.config.adapter.collection_name + "(hiduke,kingaku,bikou) values('" + collection.hiduke + "'," + collection.kingaku + ",'" + collection.bikou + "')";
                db = Ti.Database.open(collection.config.adapter.db_name);
                db.execute(sql);
                db.close();
            },
            modifyData: function() {
                var collection = this;
                var sql = "update " + collection.config.adapter.collection_name + " set hiduke= '" + collection.hiduke + "',kingaku=" + collection.kingaku + ",bikou='" + collection.bikou + "' where id =" + Alloy.Globals.id;
                db = Ti.Database.open(collection.config.adapter.db_name);
                db.execute(sql);
                db.close();
            },
            deleteData: function() {
                var collection = this;
                var sql = "delete from " + collection.config.adapter.collection_name + " where id =" + Alloy.Globals.id;
                db = Ti.Database.open(collection.config.adapter.db_name);
                db.execute(sql);
                db.close();
            }
        });
        return Collection;
    }
};

var Alloy = require("alloy"), _ = require("alloy/underscore")._, model, collection;

model = Alloy.M("money", exports.definition, [ function(migration) {
    migration.name = "money";
    migration.id = "20131111171213";
    migration.up = function(db) {
        db.createTable({
            columns: {
                id: "integer not null primary key",
                hiduke: "integer",
                kingaku: "text",
                bikou: "text"
            }
        });
    };
    migration.down = function(db) {
        db.dropTable();
    };
} ]);

collection = Alloy.C("money", exports.definition, model);

exports.Model = model;

exports.Collection = collection;